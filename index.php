<?php
require "bootstrap.php";
use Chatter\Models\User;
$app = new \Slim\App();

$app->get('/users', function($request, $response,$args){
   $_user = new User();
    $users = $_user->all();
    $payload = [];
    foreach($users as $usr){
         $payload[$usr->id] = [
             'id'=>$usr->id,
             'username'=>$usr->username,
             'name'=>$usr->name,
             'popular'=>$usr->popular
         ];
    }
    return $response->withStatus(200)->withJson($payload);
 });

$app->get('/users/image/{user_id}', function($request, $response, $args) {
    $id = $args['user_id'];
    $user = User::find($id);
    $file = $user->profile_pic;
    $img = '<img height="30" width="30" src="data:image/jpg;base64,' .  base64_encode($file)  . '" />';
    return $response->withStatus(200)->write($img);
});

$app->post('/users', function($request, $response, $args){
    $name = $request->getParsedBodyParam('name','');
    $username = $request->getParsedBodyParam('username','');
    $password = $request->getParsedBodyParam('password','');
    //$profile_pic = $request->getParsedBodyParam('profile_pic','');
    $profile_pic = $request->getUploadedFiles('profile_pic','');
    $_user = new User();
    $_user->name = $name;
    $_user->username = $username; 
    $_user->password = $password;
    $_user->profile_pic = $profile_pic;
    $_user->save();

    if($_user->id){
         $payload = ['user id: '=>$_user->id];
         return $response->withStatus(201)->withJson($payload);
     }
     else{
         return $response->withStatus(400);
     }
  });

  
 /* $app->post('/image', function ($request, $response) {
  
      $files = $request->getUploadedFiles();
      $file = $files['image_image']; // uploaded file
  
      $parameters = $request->getParams(); // Other POST params
  
      $path = "/..../uploads/"."img-".date("Y-m-d-H-m-s").".jpg";
  
      if ($file->getError() === UPLOAD_ERR_OK) {
  
          $file->moveTo($path); // Save file
  
          // DB interactions here...
  
          $sql = "INSERT INTO users (profile_pic) VALUES (:profile_pic)";
  
          $sth = $this->db->prepare($sql);
          $sth->bindParam("profile_pic", $input['profile_pic']);    
          //$sth->bindParam("item_image", $input['item_image']); 
  
          // if statement is executed successfully, return id of the last inserted restaraunt
          if ($sth->execute()) {
  
              return $response->withJson($this->db->lastInsertId());
  
          } else {
  
              // else throw exception - Slim will return 500 error
              throw new \Exception('Failed to persist restaraunt');
  
          }
  
      } else {
  
          throw new \Exception('File upload error');
  
      }
  
  });*/

 /* $app->post('/api/upload/{displayName}', function ($request, $response, $args){
    $displayName = $request->getAttribute('displayName');
    if (!isset($_FILES['uploads'])) {
        echo "Select a file !";
        return false;
    }
    $uploaddir = $_SERVER["DOCUMENT_ROOT"]."/server/assets/";
    $files = $_FILES['uploads'];
    $filename = $files['name'];
    $name = uniqid('img-'.date('Ymd')).$filename;
    $uploadfile = $uploaddir.$name;

    if (move_uploaded_file($files['tmp_name'], $uploadfile)) {
        $sql = "UPDATE users SET photo = :photo_url WHERE id = :displayName";;
        try {
            $db = new db();
            $db = $db->connect();
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':displayName', $displayName);
            $stmt->bindParam(':photo_url', $name);
            $stmt->execute();
            $db = null;
            echo "Successfully Uploaded !";
        } catch (Exception $e) {
            echo '{"error" : {"text" : '.$e->getMessage().'}}';
        }
    }else{
        echo "Sorry Upload failed !";
    }
});*/

$app->get('/users/{id}', function($request, $response,$args){
    $_id = $args['id'];
    $user = User::find($_id);
    $payload = [];
    //foreach($user as $usr){
    $payload[$user->id] = [
        'id'=>$user->id,
        'username'=>$user->username,
        'name'=>$user->name,
        'popular'=>$user->popular
    ];
    //}
    return $response->withStatus(200)->withJson($payload);
});

$app->put('/users/{id}', function($request, $response, $args){
    $popular = $request->getParsedBodyParam('popular','');
    $_user = User::find($args['id']);
    $_user->popular = $popular;
   
    if($_user->save()){
        $payload = ['user_id' => $_user->id,"result" => "The user has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});


$app->post('/login', function($request, $response,$args){
    $name  = $request->getParsedBodyParam('username','');
    $password = $request->getParsedBodyParam('password','');    
    $_user = User::where('username', '=', $name)->where('password', '=', $password)->get();
    
    if($_user[0]->id){
        $payload = ['token'=> $_user[0]->id];
        return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }

});


/*$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});*/

 $app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();